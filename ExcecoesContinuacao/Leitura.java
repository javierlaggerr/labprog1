import java.io.*;
import java.util.Scanner;

public class Leitura {

	public void leInputStreamArquivo(String arquivo) throws IOException {
		
		/*
		 * InputStream, InputStreamReader e BufferedReader
		 * 
		 * Para ler um byte de um arquivo, vamos usar o leitor de arquivo, 
		 * o FileInputStream. Para um FileInputStream conseguir ler um byte, 
		 * ele precisa saber de onde ele deverá ler. Essa informação é tão 
		 * importante que quem escreveu essa classe obriga você a passar o 
		 * nome do arquivo pelo construtor: sem isso o objeto não pode ser 
		 * construído.
		 * 
		 * A classe InputStream é abstrata e FileInputStream uma de suas 
		 * filhas concretas. FileInputStream vai procurar o arquivo no 
		 * diretório em que a JVM fora invocada (no caso do Eclipse, vai ser 
		 * a partir de dentro do diretório do projeto). Alternativamente você 
		 * pode usar um caminho absoluto.
		 * 
		 * Quando trabalhamos com java.io, diversos métodos lançam IOException, 
		 * que é uma exception do tipo checked - o que nos obriga a tratá-la 
		 * ou declará-la. Nos exemplos aqui, estamos declarando IOException 
		 * através da clausula throws do main apenas para facilitar o exemplo. 
		 * Caso a exception ocorra, a JVM vai parar, mostrando a stacktrace. 
		 * Esta não é uma boa prática e em uma aplicação real sempre trate 
		 * suas exceptions para sua aplicação poder abortar elegantemente.
		 */
		InputStream is = new FileInputStream(arquivo);
		
		/*
		 * Com um passe de mágica, passamos a ler do teclado em vez de um 
		 * arquivo, utilizando o System.in, que é uma referência a um 
		 * InputStream o qual, por sua vez, lê da entrada padrão.
		 */
		//InputStream is = System.in;
		
		/*
		 * Para recuperar um caractere, precisamos traduzir os bytes com o 
		 * encoding dado para o respectivo código unicode, isso pode usar 
		 * um ou mais bytes. Escrever esse decodificador é muito complicado, 
		 * quem faz isso por você é a classe InputStreamReader.
		 * 
		 * O construtor de InputStreamReader pode receber o encoding a ser 
		 * utilizado como parâmetro, se desejado, tal como UTF-8 ou ISO-8859-1.
		 * 
		 * InputStreamReader é filha da classe abstrata Reader, que possui 
		 * diversas outras filhas - são classes que manipulam chars.
		 */
		InputStreamReader isr = new InputStreamReader(is);
		
		/*
		 * Apesar da classe abstrata Reader já ajudar no trabalho de 
		 * manipulação de caracteres, ainda seria difícil pegar uma String. 
		 * A classe BufferedReader é um Reader que recebe outro Reader pelo 
		 * construtor e concatena os diversos chars para formar uma String 
		 * através do método readLine.
		 * 
		 * Como o próprio nome diz, essa classe lê do Reader por pedaços 
		 * (usando o buffer) para evitar realizar muitas chamadas ao sistema 
		 * operacional. Você pode até configurar o tamanho do buffer pelo 
		 * construtor.
		 */
		BufferedReader br = new BufferedReader(isr);
        
		/*
		 * Aqui, lemos apenas a primeira linha do arquivo. O método readLine 
		 * devolve a linha que foi lida e muda o cursor para a próxima linha.
		 */
		String s = br.readLine();
        
		/*
		 * Caso ele chegue ao fim do Reader (no nosso caso, fim do arquivo), 
		 * ele vai devolver null. Então, com um simples laço, podemos ler o 
		 * arquivo por inteiro.
		 */
		while (s != null) {
        	System.out.println(s);
            s = br.readLine();
		}
		br.close();
	}
	
	public void leScannerArquivo(String arquivo) throws IOException {
		
		File f = new File(arquivo);
		Scanner s = new Scanner(f);
		
		while (s.hasNextLine()) {
			System.out.println(s.nextLine());
		}
		s.close();
	}
	
	public void leFileReader(String arquivo) throws IOException {
		
		FileReader fr = new FileReader(arquivo); 
		char [] a = new char[50];
		fr.read(a);
		for(char c : a)
			System.out.print(c);
		fr.close();
	}
	
	public boolean excecoes(int numero) {
		
		// divisão por zero
		int i = 5571;
        //i = i / 0;
        System.out.println("O resultado " + i);
        
        // null pointer
        Object c = null;
        //System.out.println("C " + c.toString());
        
        /*
         * Lançando exceções
         * -------- --------
         * 
         * Podemos, também, lançar uma Exception, o que é extremamente
         * útil. Dessa maneira, resolvemos o problema de alguém poder 
         * esquecer de fazer um if no retorno de um método.
         *  
         * A palavra chave throw, que está no imperativo, lança uma 
         * Exception. Isto é bem diferente de throws, que está no 
         * presente do indicativo, e que apenas avisa da possibilidade 
         * daquele método lançá-la, obrigando o outro método que vá 
         * utilizar deste de se preocupar com essa exceção em questão. 
         * 
         * No nosso caso, lançamos uma exceção do tipo unchecked. 
         * RuntimeException é a exception mãe de todas as exceptions 
         * unchecked. A desvantagem, aqui, é que ela é muito genérica; 
         * quem receber esse erro não saberá dizer exatamente qual foi 
         * o problema. Podemos então usar uma Exception mais específica
         * como IllegalArgumentException.
         * 
         * IllegalArgumentException diz um pouco mais: algo foi passado 
         * como argumento e seu método não gostou. Ela é uma Exception 
         * unchecked pois estende de RuntimeException e já faz parte da 
         * biblioteca do java. (IllegalArgumentException é a melhor 
         * escolha quando um argumento sempre é inválido como, por 
         * exemplo, números negativos, referências nulas, etc).
         */
        if (numero > 0) {
        	return true;
        } else {
        	return false;
        	//throw new RuntimeException();
        	//throw new IllegalArgumentException();
        	/*
        	 * Podíamos melhorar ainda mais e passar para o construtor 
        	 * da IllegalArgumentException o motivo da exceção:
        	 */
        	//throw new IllegalArgumentException("Número negativo!");
        	//throw new NumeroNegativoException("Número negativo exception!");
        }
	}
}
