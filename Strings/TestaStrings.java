
public class TestaStrings {
	/*
	 * Strings
	 * =======
	 *
	 * Documentação oficial: https://docs.oracle.com/javase/8/docs/api/java/lang/String.html
	 * 
	 * Uma String armazena uma sequência de caracteres. Apesar de seu uso ser
	 * trivial, algumas de suas características podem não ser tão óbvias em um
	 * primeiro contato.
	 *  
	 * Por exemplo, um objeto String é imutável. Isto significa que seu texto 
	 * nunca é alterado. Sempre que um texto precisa ser modificado uma nova 
	 * posição na memória é alocada para que a nova String seja criada contendo
	 * o conteúdo atualizado.
	 */

	public static void main(String[] args) {
		
		/*
		 * Para o compilador, qualquer texto entre aspas duplas é uma String. 
		 * Por esse motivo, ao instanciar um objeto desse tipo, não é necessário
		 * utilizar o operador 'new'. Assim, uma String é criada de forma 
		 * semelhante a um tipo primitivo, utilizando-se a sintaxe 
		 * 
		 * 	<tipo> <nome> = <valor>, 
		 * 
		 * apesar de se tratar de um tipo por referência - um nome para um objeto
		 * em memória.
		 */
		
		String texto = "Programa para testar a classe Pessoa";
		System.out.println(texto);

		/*
		 * Toda vez que a interação a seguir for executada, um novo objeto do
		 * tipo String será instanciado. No final das 10 iterações, 10 objetos
		 * foram criados, porem a variável 'novo_texto' só fará referência para
		 * a última instância.
		 */

		for (int i = 0; i < 10; i++) {
			String novo_texto = new String("Instanciando objeto " + i);
			System.out.println(novo_texto);
		}
		
		/*
		 * É possível declarar uma String sem inicializá-la dentro de uma classe,
		 * como foi feito na classe Pessoa. Neste caso, o valor 'null' é atribuído
		 * a variável.
		 * 
		 * Quando a instância da classe Pessoa é criada, o valor padrão 'null' é
		 * atribuído a variável 'nome' e, por esse motivo, é possível imprimir o
		 * texto. 
		 */
		
		Pessoa p = new Pessoa();
		System.out.println(p.nome);
		
		/*
		 * Caso a variável seja declarada dentro de um método, como uma variável
		 * local, acontece um erro de compilação quando tentarmos imprimir esta
		 * variável. Isto acontece pois não é possível utilizar uma variável local
		 * sem antes fazer sua inicialização.
		 */
		
		String sem_texto;
		System.out.println("Imprimir variável sem inicializá-la:");
		//System.out.println(sem_texto);
		
		/*
		 * Testando igualdade entre Strings
		 * ======== ========= ===== =======
		 * 
		 * A comparação entre Strings utilizando o operador de igualdade (==) 
		 * retornará 'true' se as duas referências apontarem para o mesmo objeto 
		 * na memória.
		 */
		
		String nome = "Antônio";
		String apelido = nome;
		
		if (nome == apelido) {
			System.out.println("Igualdade: Nome e apelido são iguais!");
		}
		
		/*
		 * O mesmo acontece no a seguir, no qual temos duas vezes a ocorrência do
		 * literal "Antônio". A JVM tratará como sendo o mesmo objeto, otimizando 
		 * o uso da memória. Uma vez que nome e apelido apontam para o mesmo 
		 * objeto, a expressão lógica nome == apelido será avaliada como 'true'.
		 */

		String novo_nome = "Antônio";
		String novo_apelido = "Antônio";
		
		if (novo_nome == novo_apelido) {
			System.out.println("Igualdade: Nome e apelido são iguais!");
		}

		/*
		 * No código abaixo, nome e apelido apontam para objetos diferentes, sendo
		 * o primeiro criado com o comando new String("Maria") e o segundo com o 
		 * literal "Maria". Como mencionado anteriormente, o operador 'new' força
		 * a criação de um novo objeto.
		 * 
		 * Neste caso, a comparação entre as variáveis será avaliada como 'false'
		 * e o programa irá imprimir "Nome e apelido NÃO são iguais".
		 */
		
		String outro_nome = new String("Maria");
		String outro_apelido = "Maria";
		
		if (outro_nome == outro_apelido) {
			System.out.println("Igualdade: Nome e apelido são iguais!");
		} else {
			System.out.println("Igualdade: Nome e apelido NÃO são iguais!");
		}
		
		/*
		 * Quando estivermos comparando dois textos estaremos interessados em 
		 * saber se seus caracteres são iguais e não se estão armazenados no 
		 * mesmo espaço em memória. Nesse caso devemos utilizar um dos métodos de 
		 * comparação da classe String.
		 */
		
		/*
		 * Equals
		 * ======
		 * 
		 * Equals é um método da classe Object utilizado para testar a relação 
		 * de igualdade entre dois objetos. Esse método está presente em todas as 
		 * classes, já que todas elas derivam de Object. A forma como essa 
		 * comparação será feita pode ser determinada por quem está escrevendo a 
		 * classe e no caso de String, o método equals verifica se duas Strings 
		 * contém exatamente os mesmos caracteres.
		 */
		
		String nome1 = "Carlos";
		String nome2 = "Carla";
		
		if (nome1.equals(nome2)) {
			System.out.println("equals: nome1 é igual ao nome2");
		} else {
			System.out.println("equals: nome1 NÃO é igual ao nome2");
		}
		
		/*
		 * EqualsIgnoreCase
		 * ================
		 * 
		 * O método equalsIgnoreCase ignora a distinção entre letras maiúsculas
		 * e minúsculas quando compara duas Strings.
		 */
		
		String nome3 = "Carlos";
		String nome4 = "carlos";
		
		if (nome3.equalsIgnoreCase(nome4)) {
			System.out.println("equalsIgnoreCase: nome3 é igual ao nome4");
		} else {
			System.out.println("equalsIgnoreCase: nome3 NÃO é igual ao nome4");
		}
		
		/*
		 * CompareTo
		 * =========
		 * 
		 * Esse método pode retornar 0 se as strings forem iguais, um número 
		 * negativo se a String que invoca o compareTo for menor que a String 
		 * que é passada como um argumento e um número positivo se a String que 
		 * invoca o compareTo for maior que a String que é passada como argumento.
		 */
		
		String nome5 = "Carlos";
		String nome6 = "Carla";
		         
		System.out.println("compareTo: nome6.compareTo(nome5) = " + nome6.compareTo(nome5));
		System.out.println("compareTo: nome5.compareTo(nome6) = " + nome5.compareTo(nome6));

		/*
		 * CompareToIgnoreCase
		 * ===================
		 * 
		 * Para ignorar a distinção entre maiúsculas e minúsculas, o método 
		 * compareToIgnoreCase compara textos lexigraficamente. No exemplo abaixo
		 * sao feitas duas comparações, uma usando compareTo e a outra usando 
		 * compareToIgnoreCase para analisarmos as diferenças.
		 */
		
		String descricao = "A API de Strings é uma das mais utilizadas na linguagem Java";
		String linguagem = descricao.substring(descricao.indexOf("Java"), descricao.length());
		 
		if (linguagem.compareToIgnoreCase("java") == 0) {
		    System.out.println("compareToIgnoreCase: Encontrei a linguagem! Ela é " + linguagem);
		} else {
			System.out.println("compareToIgnoreCase: Não encontrei a linguagem!");
		}
		 
		if(linguagem.compareTo("java") == 0) {
		    System.out.println("compareTo: Encontrei a linguagem! Ela é " + linguagem);
		} else {
			System.out.println("compareTo: Não encontrei a linguagem!");
		}
		
		System.out.println("================================================");
		System.out.println("	Alguns métodos da classe String");
		System.out.println("================================================");
		
		/*
		 * Alguns métodos da classe String
		 */
		
		/*
		 * Concat
		 * ======
		 * 
		 * Existem duas formas de unir duas ou mais sequências de caracteres. 
		 * A mais comum dentre elas é utilizando o operador de adição '+', a outra
		 * forma é utilizando o método 'concat'.
		 */
		
		String nomeCompleto = nome + " Silva";
		System.out.println("concat: " + nomeCompleto);
		
		String nomeCompleto1 = nome.concat(" Silva");
		System.out.println("concat: " + nomeCompleto1);
		
		/*
		 * ValueOf
		 * =======
		 * 
		 * O método estático valueOf da classe String converte um tipo primitivo 
		 * em um objeto do tipo String. Por ser um método estático, ele não 
		 * precisa de uma instância para ser invocado.
		 */
		
		double numero = 102939939.939;
		boolean booleano = true;
		 
		System.out.println("valueOf: " + String.valueOf(numero));
		System.out.println("valueOf: " + String.valueOf(booleano));

		/*
		 * Length
		 * ======
		 * 
		 * Retorna o comprimento do texto em uma String.
		 */
		
		String nomeCurso = "Java";
		 
		System.out.println("Length: " + nomeCurso.length());
		
		/*
		 * CharAt
		 * ======
		 * 
		 * Retorna o caractere em uma localização específica de uma String. 
		 * Esse método possui um parâmetro do tipo inteiro que é usado como 
		 * índice. É importante lembrar que o índice sempre começa a ser contado 
		 * do número 0 (zero) em diante. Sendo assim a posição do caractere a em 
		 * Carlos é 1 e não 2, como se poderia deduzir.
		 */

		if (nomeCurso.charAt(1) == 'a') {
		    System.out.println("charAt: O caractere 'a' está na posição 1");
		}
		
		/*
		 * GetChars
		 * ========
		 * 
		 * Recupera um conjunto de caracteres de uma String. Esse método possui 
		 * os seguintes parâmetros de entrada:
		 * 
		 * srcBegin: Índice do primeiro caractere da String a ser copiada.
		 * srcEnd: Índice após o último caractere a ser copiado.
		 * dst: destino do conjunto de caracteres.
		 * dstBegin: início do deslocamento no conjunto de destino.
		 */
		
		System.out.println("- - - - getChars - - - -");
		String nomeCompletoCurso = "Curso Java Web";
		char[] numIndice = new char[7];
		 
		nomeCompletoCurso.getChars(2, 9, numIndice, 0);
		System.out.print("Valores Copiados: \n");
		          
		for(char caractere : numIndice) {
		    System.out.print("["+caractere+"]");
		}
		System.out.println();
		int[] b = {0,1,2,3,4,5,6};
		for(int i = 0; i < b.length; i++) {
		    System.out.print("["+b[i]+"]");
		}
		System.out.println();
		
		/*
		 * StartsWith e endsWith
		 * ========== = ========
		 * 
		 * Os métodos startsWith e endsWith aceitam uma String e um número 
		 * inteiro como argumentos, retornando um valor booleano que indica se a 
		 * String inicia ou termina, respectivamente, com o texto informado a 
		 * partir da posição dada.
		 */
		System.out.println("- - - - startsWith e endsWith - - - -");
		System.out.println("Antônio inicia com 'Jo'? " + nome.startsWith("Jo"));
		System.out.println("Antônio inicia com 'An'? " + nome.startsWith("An"));
		System.out.println("Antônio termina com 'ao'? " + nome.endsWith("ao"));
		System.out.println("Antônio termina com 'io'? " + nome.endsWith("io"));
		
		/*
		 * IndexOf e lastIndexOf
		 * ======= = ===========
		 * 
		 * indexOf: Localiza a primeira ocorrência de um caractere em uma String.
		 * Se o método localizar o caractere, é retornado o índice do caractere 
		 * na String, caso contrário retorna -1. Existem duas versões do indexOf 
		 * que procuram caracteres em uma String.
		 * 
		 * 1ª versão – aceita um inteiro que se refere ao caractere que se deseja
		 * buscar na String.
		 * 2ª versão – aceita dois argumentos inteiros. O primeiro é o caractere 
		 * e o segundo é o índice inicial em que a pesquisa da String deve 
		 * iniciar.
		 * 
		 * lastIndexOf : Localiza a última ocorrência de um caractere em uma 
		 * String. O método procura do fim da String em direção ao começo, se 
		 * encontrar o caractere é retornado o seu índice na String, caso 
		 * contrário retorna -1. Existem duas versões do lastIndexOf que pesquisam 
		 * por caracteres em uma String.
		 * 
		 * 1ª versão – utiliza um inteiro que se refere ao caractere.
		 * 2ª versão – aceita 2 argumentos, sendo o primeiro um inteiro que se
		 * refere ao caractere e o segundo, o índice a partir do qual deve iniciar
		 * a pesquisa de trás para frente.
		 */
		System.out.println("- - - - IndexOf e lastIndexOf - - - -");
		String letras = "abcadefghijklmcopqrsdeftuvz";

		System.out.printf("indexOf: Último 'c' está localizado no index %d\n", letras.indexOf('c'));
		System.out.printf("indexOf: Último 'a' está localizado no index %d \n", letras.indexOf('a', 1));
		 
		System.out.printf("indexOf: '$' está localizado no index %d\n", letras.indexOf('$'));
		 
		System.out.printf("indexOf: Último 'c' está localizado no index %d\n", letras.lastIndexOf('c'));
		System.out.printf("indexOf: Último 'a' está localizado no index %d\n", letras.lastIndexOf('a', 5));
		System.out.printf("indexOf: Último '$' está localizado no index %d\n", letras.lastIndexOf('$'));
		 
		System.out.printf("indexOf: \"def\" está localizado no index %d\n", letras.indexOf("def"));
		 
		System.out.printf("indexOf: \"def\" está localizado no index %d\n", letras.indexOf("def", 7)); 
		System.out.printf("indexOf: \"hello\" está localizado no index %d\n\n", letras.indexOf("hello"));
		
		/*
		 * Substring
		 * =========
		 * 
		 * Permite extrair substrings de Strings e fornece 2 métodos substring 
		 * para permitir que um novo objeto seja criado copiando parte de um 
		 * objeto String existente. Cada método retorna um novo objeto desse tipo. 
		 * Existem duas versões desse método que são:
		 * 
		 * 1ª versão – recebe um argumento do tipo inteiro, que especifica a 
		 * partir de que caractere a cópia deve começar. A substring retornada 
		 * contém uma cópia dos caracteres desde esse índice até o último 
		 * caractere na String.
		 * 
		 * 2ª versão – recebe dois argumentos do tipo inteiro. Sendo o primeiro a 
		 * posição do primeiro caractere a ser copiado e o segundo a posição do 
		 * último caractere a ser copiado.
		 */
		System.out.println("- - - - Substring - - - -");
		String jose = "José Silveira";
		System.out.println("String : " + jose);
		 
		String substring = jose.substring(5);
		System.out.println("String depois da 3º index: " + "["+substring+"]");
		 
		substring = jose.substring(1, 6);
		System.out.println("Substring (1,6): " + "["+substring+"]");
		
		/*
		 * Replace
		 * =======
		 * 
		 * Retorna um novo objeto contendo a String original com um trecho 
		 * especificado substituído por outra expressão indicada. Esse método 
		 * deixa a String original inalterada.
		 */
		System.out.println("- - - - Repalce - - - -");
		String nomeAlterado = jose.replace('e', 'o');
		System.out.println("Nome alterado: " + nomeAlterado);
		
		/*
		 * ToUpperCase e toLowerCase
		 * =========== = ===========
		 * 
		 * toUpperCase: Retorna uma nova String com o conteúdo da original 
		 * convertido para letras maiúsculas, mantendo a original inalterada.
		 * 
		 * toLowerCase: De forma semelhante ao anterior, o toLowerCase retorna 
		 * uma cópia de uma String com todas as letras convertidas para minúsculo,
		 * mantendo a original inalterada.
		 */
		System.out.println("- - - - toUpperCase e toLowerCase - - - -");
		String nomeA = "joaquina";
		String nomeB = "Paulo";
		 
		System.out.println(nomeA.toUpperCase());
		System.out.println(nomeB.toLowerCase());
		
		/*
		 * Trim
		 * ====
		 * 
		 * Gera um novo objeto String, removendo todos os caracteres de espaço 
		 * em branco que aparecem no início ou no fim da String em que o método 
		 * opera. O método retorna um novo objeto String contendo a String sem 
		 * espaço em branco no inicio ou no fim. A String original permanece 
		 * inalterada.
		 * 
		 * ToCharArray
		 * ===========
		 * 
		 * Cria um novo conjunto de caracteres que contém uma cópia dos 
		 * caracteres da variável apontada.
		 */
		System.out.println("- - - - Trim e toCharArray - - - -");
		String s1 = "olá";
		String s2 = "TCHAU";
		String s3 = " espaços ";
		 
		System.out.println("s1 = "+ s1 + "\n" + "s2 = "+ s2 + "\n" + "s3 = "+s3);
		 
		System.out.printf("Replace 'l' por 'L' no s1: %s\n\n", s1.replace('l', 'L'));
		 
		System.out.printf("s1.toUpperCase() = %s\n", s1.toUpperCase());
		System.out.printf("s2.toUpperCase() = %s\n\n", s2.toLowerCase());
		
		System.out.printf("s3 depois do trim = \"%s\"\n\n", s3.trim());
		 
		char[] charArray = s1.toCharArray(); 
		System.out.printf("s1 como um caracter array = ");
		
		for(char caracter : charArray) {
		    System.out.print(caracter);
		}
	}
}
