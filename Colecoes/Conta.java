
public class Conta  implements Comparable<Conta>{


	protected static int totalDeContas;
	protected int numero;
	protected String nome;
	protected double saldo;
	protected String tipo;
	

	public Conta() {
		Conta.totalDeContas = Conta.totalDeContas + 1;
	}
	
	public void deposito(double valor) {
		this.saldo = valor;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public double getSaldo() {
		return this.saldo;
	}

	public String getTipo() {
		return this.tipo;
	}

	public static int getTotalDeContas() {
		return Conta.totalDeContas;
	}
	
	/*
	 * Sempre que falamos em ordenação, precisamos pensar em um critério de 
	 * ordenação, uma forma de determinar qual elemento vem primeiro. É 
	 * necessário instruir o método sort sobre como comparar nossos objetos do 
	 * tipo Conta a fim de determinar uma ordem na lista. Para isto, o método 
	 * sort necessita que todos seus objetos da lista sejam comparáveis e 
	 * possuam um método que se compara com outra Conta. Como o método sort terá
	 * a garantia de que a sua classe possui esse método? Isso será feito, 
	 * novamente, através de um contrato, de uma interface!
	 */
	
	public int compareTo(Conta outra) {
		if (this.saldo < outra.saldo) {
			return -1;
		}

		if (this.saldo > outra.saldo) {
			return 1;
		}
		return 0; 
	}
	
	/*
	 * Com o código anterior, nossa classe tornou-se “comparável": 
	 * dados dois objetos da classe, conseguimos dizer se um objeto é maior, 
	 * menor ou igual ao outro, segundo algum critério por nós definido. No 
	 * nosso caso, a comparação será feita baseando-se no saldo da conta.
	 * 
	 * Repare que o critério de ordenação é totalmente aberto, definido pelo 
	 * programador. Se quisermos ordenar por outro atributo (ou até por uma 
	 * combinação de atributos), basta modificar a implementação do método 
	 * compareTo na classe.
	 * 
	 * Quando chamarmos o método sort de Collections, ele saberá como fazer a 
	 * ordenação da lista usando o critério que definimos no método compareTo.
	 */
	
	/*
	 * Conforme o contrato geral de hashCode(), que consta na especificação da 
	 * classe Object, se dois objetos são diferentes de acordo com equals() 
	 * então não é obrigatório que seus códigos hash sejam diferentes.
	 * 
	 * Portanto, objetos que retornam o mesmo código hash não são 
	 * necessariamente iguais. Assim, quando encontra no conjunto um objeto com 
	 * o mesmo código hash do objeto a ser inserido, HashSet faz uma chamada ao 
	 * método equals() para verificar se os dois objetos são iguais. Dessa 
	 * forma, a classe Conta deve sobrescrever o método equals() também.
	 */
	public boolean equals(Object o) {
		Conta outra = (Conta) o;
		return this.nome.equals(outra.getNome());
	}
	
	public int hashCode() {
		return this.nome.hashCode();
	}
}
