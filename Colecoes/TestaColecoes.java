import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;


/*
 * Collections
 * -----------
 * 
 * Collections Framework é um conjunto bem definido de interfaces e classes para
 * representar e tratar grupos de dados como uma única unidade, que pode ser 
 * chamada coleção, ou collection. A Collections Framework contém os seguintes 
 * elementos:
 * 
 * - Interfaces: tipos abstratos que representam as coleções. Permitem que 
 * coleções sejam manipuladas tendo como base o conceito “Programar para 
 * interfaces e não para implementações”, desde que o acesso aos objetos se 
 * restrinja apenas ao uso de métodos definidos nas interfaces;
 * 
 * - Implementações: são as implementações concretas das interfaces;
 * 
 * - Algoritmos: são os métodos que realizam as operações sobre os objetos das 
 * coleções, tais como busca e ordenação.
 * 
 * Interfaces
 * ----------
 * 
 * A lista abaixo apresenta uma breve descrição de cada uma das interfaces de 
 * sua hierarquia:
 * 
 * - Collection – está no topo da hierarquia. Não existe implementação direta 
 * dessa interface, mas ela define as operações básicas para as coleções, como 
 * adicionar, remover, esvaziar, etc.;
 * 
 * - Set – interface que define uma coleção que não permite elementos 
 * duplicados. A interface SortedSet, que estende Set, possibilita a 
 * classificação natural dos elementos, tal como a ordem alfabética;
 * 
 * List – define uma coleção ordenada, podendo conter elementos duplicados. 
 * Em geral, o usuário tem controle total sobre a posição onde cada elemento é 
 * inserido e pode recuperá-los através de seus índices. Prefira esta interface 
 * quando precisar de acesso aleatório, através do índice do elemento;
 * 
 * - Queue – um tipo de coleção para manter uma lista de prioridades, onde a 
 * ordem dos seus elementos, definida pela implementação de Comparable ou 
 * Comparator, determina essa prioridade. Com a interface fila pode-se criar 
 * filas e pilhas;
 * 
 * - Map – mapeia chaves para valores. Cada elemento tem na verdade dois 
 * objetos: uma chave e um valor. Valores podem ser duplicados, mas chaves não. 
 * SortedMap é uma interface que estende Map, e permite classificação ascendente
 * das chaves. Uma aplicação dessa interface é a classe Properties, que é usada 
 * para persistir propriedades/configurações de um sistema, por exemplo.
 * 
 * A API oferece também interfaces que permitem percorrer uma coleção derivada 
 * de Collection, como, por exemplo:
 * 
 * - Iterator – possibilita percorrer uma coleção e remover seus elementos;
 * 
 * - ListIterator – estende Iterator e suporta acesso bidirecional em uma lista,
 * modificando e/ou removendo elementos.
 * 
 * Implementações
 * --------------
 */
public class TestaColecoes {

	public static void main(String[] args) {

		/*
		 * A interface List
		 * - --------- ----
		 * 
		 * A estrutura de dados lista é utilizada para armazenar conjuntos de 
		 * elementos. A vantagem em utilizar listas no lugar de vetores é o 
		 * fato de as listas serem alocadas dinamicamente de forma que não 
		 * precisamos prever seu tamanho máximo. As implementações de lista
		 * dentro de Collections são ArrayList e LinkedList.
		 * 
		 * ArrayList oferece acesso aleatório rápido através do índice. Já em 
		 * LinkedList o acesso aleatório é lento e necessita de um objeto nó 
		 * para cada elemento, que é composto pelo dado propriamente dito e uma 
		 * referência para o próximo nó, ou seja, consome mais memória. Além 
		 * dessas considerações, se for necessário inserir elementos no início 
		 * e deletar elementos no interior da lista, a melhor opção poderia ser 
		 * LinkedList. Para apoiar a decisão de usar uma ou outra implementação 
		 * é melhor fazer testes de desempenho. 
		 * 
		 * ArrayList
		 * ---------
		 * 
		 * O ArrayList é como um array cujo tamanho pode crescer. A busca de um 
		 * elemento é rápida, mas inserções e exclusões não são. Podemos afirmar
		 * que as inserções e exclusões são lineares, o tempo cresce com o 
		 * aumento do tamanho da estrutura. Esta implementação é preferível 
		 * quando se deseja acesso mais rápido aos elementos. Por exemplo, se 
		 * você quiser criar um catálogo com os livros de sua biblioteca pessoal
		 * e cada obra inserida receber um número sequencial (que será usado 
		 * para acesso) a partir de zero;
		 * 
		 * Para trabalharmos com a classe ArrayList precisamos conhecer seus
		 * métodos, listados a seguir:
		 * 
		 * - public ArrayList(): cria uma lista vazia do tipo ArrayList.
		 * - public boolean add(<elemento>): adiciona um elemento no fim da 
		 *   lista. 
		 * - public void add(índice, <elemento>): adiciona um elemento na 
		 *   posição índice.
		 * - public <elemento> get(int índice): obtém o elemento de índice. 
		 * - public <elemento> remove(int index): retorna o elemento de índice 
		 *   e o elimina da lista.
		 * - public boolean isEmpty(): verifica se a lista está vazia.
		 * - public boolean contains(Object o): indica se o elemento existe na
		 *   lista.
		 * 
		 * Para navegarmos em uma lista, utilizaremos a interface Iterator. 
		 * Seguem os principais métodos de Iterator:
		 * 
		 * - boolean hasNext(): verifica se existe próximo elemento na lista;
		 * - next(): obtém o elemento sob o Iterator e avança para o próximo 
		 *   elemento;
		 * - void remove(): remove o elemento sob o Iterator.
		 * 
		 * O ArrayList pode armazenar qualquer tipo de objeto. Assim, quando 
		 * obtemos um objeto, o retorno é uma instância do tipo Object. Por isso
		 * é necessário fazer a conversão explícita para o tipo desejado no 
		 * retorno.
		 * 
		 * ==> ArrayList não é um Array! <==
		 * 
		 * É comum confundir uma lista do tipo ArrayList com um array, porém ela
		 * não é um array.O que ocorre é que, internamente, ela usa um array
		 * como estrutura para armazenar os dados, porém este atributo está
		 * propriamente encapsulado e nã há como acessá-lo. Repare, também, que 
		 * não é possível indexar elementos de um lista do tipo ArrayList usando
		 * [], nem acessar o atributo length. Não há relação!
		 */
		
		/*
		 * Em listas do tipo ArrayList, é possível colocar qualquer Object. Com 
		 * isso, é possível misturar objetos.
		 * 
		 * Porém, quando precisamos recuperar esses objetos, como saber seu 
		 * tipo? Como o método get devolve um Object, precisamos fazer o cast.
		 * Mas com uma lista com vários objetos de tipos diferentes, isso pode 
		 * não ser tão simples.
		 * 
		 * Geralmente, não nos interessa uma lista com vários tipos de objetos 
		 * misturados. No Java 5.0, podemos usar o recurso de Generics para 
		 * restringir as listas a um determinado tipo de objetos (e não qualquer
		 * Object).
		 */
		System.out.println();
		System.out.println("- - - - - Listas - - - - -");
		System.out.println();

		List<Conta> lista = new ArrayList();
		
		Conta lista_conta = new ContaCorrente(1, "José", 100);
		lista.add(lista_conta);
		
		//c = new ContaPoupanca(2,"João");
		//lista.add(0,lista_conta); 
		
		lista_conta = new ContaCorrente(2,"João", 50);
		lista.add(lista_conta);
		
		lista_conta = new ContaCorrente(3,"Maria", 200);
		lista.add(lista_conta);
		
		ContaPoupanca p = new ContaPoupanca(3,"Maria");
		p.saldo = 200;
		//lista.add(p);
		int x = lista_conta.compareTo(p);
		System.out.println("x: " + x);
		
		Iterator i = lista.iterator();
		while(i.hasNext()) {
			lista_conta = (Conta) i.next();
			System.out.println("Lista: Numero da Conta: " + lista_conta.getNumero());
			System.out.println("Lista: Nome na Conta: " + lista_conta.getNome());
		}
		System.out.println();
		/*
		 * Acesso aleatório e percorrendo listas com get
		 * ------ --------- - ----------- ------ --- ---
		 * 
		 * Algumas listas, como a ArrayList, têm acesso aleatório aos seus 
		 * elementos: a busca por um elemento em uma determinada posição é feita
		 * de maneira imediata, sem que a lista inteira seja percorrida (que 
		 * chamamos de acesso sequencial).
		 * 
		 * Neste caso, o acesso através do método get(int) é muito rápido. Ao
		 * contrário, percorrer uma lista usando um for pode ser desastroso. 
		 * Ao percorrermos uma lista, devemos usar sempre um Iterator ou  um 
		 * enhanced for.
		 */
		
		/*
		 * Ordenação: Collections.sort
		 * 
		 * Vimos anteriormente que as listas são percorridas de maneira 
		 * pré-determinada de acordo com a inclusão dos itens. Mas, muitas 
		 * vezes, queremos percorrer a nossa lista de maneira ordenada.
		 * 
		 * A classe Collections traz um método estático sort que recebe um 
		 * List como argumento e o ordena por ordem crescente.
		 */
		
		Collections.sort(lista);
		
		i = lista.iterator();
		while(i.hasNext()) {
			lista_conta = (Conta) i.next();
			System.out.println("Lista: Numero da Conta: " + lista_conta.getNumero());
			System.out.println("Lista: Nome na Conta: " + lista_conta.getNome());
		}
		
		/*
		 * A interface Set
		 * - --------- ---
		 * 
		 * Uma das características de List é que ela permite elementos 
		 * duplicados, o que não é desejável em nossa lista de clientes. A 
		 * interface Set é composta pelas seguintes implementações: HashSet, 
		 * TreeSet e LinkedHashSet. 
		 * 
		 * - HashSet – o acesso aos dados é mais rápido que em um TreeSet, mas 
		 * nada garante que os dados estarão ordenados. Escolha este conjunto 
		 * quando a solução exigir elementos únicos e a ordem não for 
		 * importante. Poderíamos usar esta implementação para criar um catálogo
		 * pessoal das canções da nossa discografia;
		 * 
		 * - TreeSet – os dados são classificados, mas o acesso é mais lento que
		 * em um HashSet. Se a necessidade for um conjunto com elementos não 
		 * duplicados e acesso em ordem natural, prefira o TreeSet. É 
		 * recomendado utilizar esta coleção para as mesmas aplicações de 
		 * HashSet, com a vantagem dos objetos estarem em ordem natural;
		 * 
		 * - LinkedHashSet – é derivada de HashSet, mas mantém uma lista 
		 * duplamente ligada através de seus itens. Seus elementos são iterados 
		 * na ordem em que foram inseridos. Opcionalmente é possível criar um 
		 * LinkedHashSet que seja percorrido na ordem em que os elementos foram 
		 * acessados na última iteração. Poderíamos usar esta implementação para
		 * registrar a chegada dos corredores de uma maratona;
		 * 
		 */
		System.out.println();
		System.out.println("- - - - - Conjuntos - - - - -");
		System.out.println();
		
		Set<Conta> conjunto = new HashSet();
		//Set<Conta> conjunto = new TreeSet();
		
		Conta conjunto_conta1 = new ContaCorrente(1, "José", 100);		
		Conta conjunto_conta2 = new ContaCorrente(2,"João", 50);
		Conta conjunto_conta3 = new ContaCorrente(3,"Maria", 200);
		
		conjunto.add(conjunto_conta1);
		conjunto.add(conjunto_conta2);
		conjunto.add(conjunto_conta3);
		
		System.out.println(conjunto);
		System.out.println();
		
		/*
		 * HashSet usa o código hash do objeto – dado pelo método hashCode() – 
		 * para saber onde deve por e onde buscar o mesmo no conjunto (Set). 
		 * Antes ele verifica se não existe outro objeto no Set com o mesmo 
		 * código hash. Se não há código hash igual, então ele sabe que o objeto
		 * a ser inserido não está duplicado. Dessa forma, classes cujas 
		 * instâncias são elementos de HashSet devem implementar o método 
		 * hashCode().
		 */
		
		System.out.println("Adicionando objeto duplicado");
		Conta conjunto_conta4 = new ContaCorrente(4,"Maria", 200);
		conjunto.add(conjunto_conta4);
		
		System.out.println(conjunto);
		
		/*
		 * Caso o programa seja desenvolvido seguindo as interfaces, é possível
		 * usar um conjunto (Set) classificado, simplesmente trocando HashSet 
		 * por TreeSet sem necessidade de alterar o restante do código, pois 
		 * tanto TreeSet como HashSet implementam exatamente os mesmos métodos 
		 * de Set. No entanto, vale ressaltar que a classe dos elementos que são
		 * adicionados ao TreeSet deve implementar Comparable. Como Conta já 
		 * implementa esta interface não precisamos nos preocupar com isso.
		 */
		
		/*
		 * A interface Map
		 * - --------- ---
		 * 
		 * Esta interface mapeia chaves para valores. Para usar uma classe que 
		 * implementa Map, quaisquer classes que forem utilizadas como chave 
		 * devem sobrescrever os métodos hashCode() e equals(). Isso é 
		 * necessário porque em um Map as chaves não podem ser duplicadas, 
		 * apesar dos valores poderem ser. A interface Map é composta pelas
		 * seguintes implementações: HashMap, TreeMap e LinkedHashMap.
		 * 
		 * - HashMap – baseada em tabela de espalhamento, permite chaves e 
		 * valores null. Não existe garantia que os dados ficarão ordenados. 
		 * Escolha esta implementação se a ordenação não for importante e 
		 * desejar uma estrutura onde seja necessário um ID (identificador);
		 * 
		 * - TreeMap – implementa a interface SortedMap. Há garantia que o mapa 
		 * estará classificado em ordem ascendente das chaves. Mas existe a 
		 * opção de especificar uma ordem diferente. Use esta implementação para
		 * um mapa ordenado. Aplicação semelhante a HashMap, com a diferença que
		 * TreeMap perde no quesito desempenho;
		 * 
		 * - LinkedHashMap – mantém uma lista duplamente ligada através de seus
		 * itens. A ordem de iteração é a ordem em que as chaves são inseridas 
		 * no mapa. Se for necessário um mapa onde os elementos são iterados na 
		 * ordem em que foram inseridos, use esta implementação.
		 */
		
		System.out.println();
		System.out.println("- - - - - Mapas - - - - -");
		System.out.println();
		
		Map<String, Conta> mapa = new TreeMap();

		/*
		 * Note que na declaração do collection informamos dois tipos: String e 
		 * Conta. O primeiro refere-se à chave e o segundo ao valor. O método 
		 * para inserir na estrutura é put(), que recebe dois objetos (chave e 
		 * valor). Para recuperar um objeto específico utilizamos o método get()
		 *  passando a chave como parâmetro.
		 */
		
		Conta mapa_conta1 = new ContaCorrente(1, "José", 100);		
		Conta mapa_conta2 = new ContaCorrente(2,"João", 50);
		Conta mapa_conta3 = new ContaCorrente(3,"Maria", 200);
		
		mapa.put("José", mapa_conta1);
		mapa.put("João", mapa_conta2);
		mapa.put("Maria", mapa_conta3);
		
		System.out.println(mapa);
		System.out.println();
		
		/*
		 * Como Map não estende Collection, não tem os métodos iterator() e 
		 * listIterator(). Entretanto, existe o método keySet() que retorna um 
		 * Set com as chaves do mapa, e o método values() que retorna um 
		 * Collection com os valores associados às chaves.
		 */
		
		Collection<Conta> contas = mapa.values();
		for (Conta c: contas) {
			System.out.println(c + ": " + c.getNome());
		}
		
	}

	/*
	 * Além das interfaces apresentadas, existem outras, tais como NavigableSet,
	 * BlockingQueue, Deque, BlockingDeque, NavigableMap, etc.
	 */
}