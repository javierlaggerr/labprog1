
/*
 * Super e Sub Classe
 * ----- - --- ------
 * 
 * A nomenclatura mais encontrada é que Funcionario é a superclasse 
 * de Gerente, e Gerente é a subclasse de Funcionario. Dizemos também 
 * que todo Gerente é um Funcionário. Outra forma é dizer que Funcionario 
 * é classe mãe de Gerente e Gerente é classe filha de Funcionario.
 * 
 * 
 */

public class Funcionario {
	protected String nome;
    protected String cpf;
    protected double salario;

    /*
     * Sobrecarga
     * ----------
     * 
     * Métodos de mesmo nome podem ser declarados na mesma classe, 
     * contanto que tenham diferentes conjuntos de parâmetros 
     * (determinado pelo número, tipos e ordem dos parâmetros). 
     * Isso é chamado sobrecarga de método.
     * 
     * Para que os métodos de mesmo nome possam ser distinguidos, 
     * eles devem possuir assinaturas diferentes. A assinatura 
     * (signature) de um método é composta pelo nome do método e 
     * por uma lista que indica os tipos de todos os seus argumentos. 
     * Assim, métodos com mesmo nome são considerados diferentes se 
     * recebem um diferente número de argumentos ou tipos diferentes 
     * de argumentos e têm, portanto, uma assinatura diferente.
     * 
     * Quando um método sobrecarregado é chamado, o compilador Java 
     * seleciona o método adequado examinando o número, os tipos e a 
     * ordem dos argumentos na chamada.
     */
    
    public Funcionario(String nome) {
    	this.nome = nome;
    }
    
    public Funcionario(String nome, String cpf) {
    	this.nome = nome;
    	this.cpf = cpf;
    }
    
    public Funcionario(String nome, String cpf, double salario) {
    	this.nome = nome;
    	this.cpf = cpf;
    	this.salario = salario;
    }
    
    public double getBonificacao() {
        return this.salario * 0.10;
    }
    
    public void setNome(String nome) {
    	this.nome = nome;
    }
    
    public void setCpf(String cpf) {
    	this.cpf = cpf;
    }
    
    public void setSalario(double salario) {
    	this.salario = salario;
    }
    
}
