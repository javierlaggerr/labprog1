
public class TestaFuncionarios {

	public static void main(String[] args) {
		
		Gerente gerente = new Gerente("Jose","123.456.789-10");
		gerente.setSalario(5000);
		System.out.println("Bonificação do gerente: " + gerente.getBonificacao());
		
		Funcionario funcionario = new Funcionario("Joao","987.654.321-01");
		funcionario.setSalario(1000);
		System.out.println("Bonificação do funcionario: " + funcionario.getBonificacao());
	}
}
