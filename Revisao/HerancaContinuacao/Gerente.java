
public class Gerente extends Funcionario{
    private int senha;
    private int numeroDeFuncionariosGerenciados;

    public Gerente(String nome) {
    	super(nome);
    }
    
    public Gerente(String nome, String cpf) {
    	super(nome,cpf);
    }
    
    public Gerente(String nome, String cpf, double salario) {
    	super(nome,cpf,salario);
    }
    
    public boolean autentica(int senha) {
        if (this.senha == senha) {
            System.out.println("Acesso Permitido!");
            return true;
        } else {
            System.out.println("Acesso Negado!");
            return false;
        }
    }
    
    /*
     * Sobrescrita
     * -----------
     * 
     * A técnica de sobrescrita permite reescrever um método em uma 
     * subclasse de forma que tenha comportamento diferente do método 
     * de mesma assinatura existente na sua superclasse.
     * 
     * A anotação @Override
     * - -------- ---------
     * 
     * Há como deixar explícito no seu código que determinado método 
     * é a reescrita de um método da sua classe mãe. Fazemos isso 
     * colocando @Override em cima do método. Isso é chamado anotação.
     * 
     * Repare que, por questões de compatibilidade, isso não é 
     * obrigatório. Mas caso um método esteja anotado com @Override, 
     * ele necessariamente precisa estar reescrevendo um método da classe 
     * mãe.
     */
    
    @Override
    public double getBonificacao() {
        return this.salario * 0.15;
    }
}
