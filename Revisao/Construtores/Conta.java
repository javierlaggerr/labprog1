
public class Conta {

	private static int totalDeContas;
	private int numero;
	private String nome;
	private double saldo;
	
	/* 
	 * Construtores
	 * ------------
	 * 
	 * Construtor é um método especial para a criação e inicialização de uma 
	 * nova instância de uma classe. Um construtor inicializa o novo objeto e 
	 * suas variáveis, cria quaisquer outros objetos de que ele precise e realiza 
	 * quaisquer outras operações de que o objeto precise para inicializar-se.
	 * 
	 * Sempre que queremos criar um novo objeto de uma determinada 
	 * classe utilizamos a palavra new acompanhada por um construtor.
	 * 
	 * O construtor de uma classe tem, por definição, o mesmo nome que a classe.
	 * 
	 * É possível definir diversos construtores para a mesma classe, tendo os 
	 * tipos ou a quantidade de parâmetros diferentes para cada um deles.
	 * 
	 * O Java define um construtor padrão para classes que não tem nenhum 
	 * construtor definido. O construtor padrão não recebe nenhum argumento.
	 * 
	 * Porém, a partir do momento em que declaramos um construtor, o construtor 
	 * padrão deixa de existir. Assim, agora que criamos esses dois construtores 
	 * é impossível criar uma nova instância de Conta sem utilizar um dos dois.
	 */
	public Conta(int numero, String nome, double saldo) {
		Conta.totalDeContas = Conta.totalDeContas + 1;
		this.numero = numero;
		this.nome = nome;
		this.saldo = saldo;
	}
	
	public Conta(int numero, String nome) {
		Conta.totalDeContas = Conta.totalDeContas + 1;
		this.numero = numero;
		this.nome = nome;
		this.saldo = 0;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
	/* 
	 * Métodos e atributos estáticos só podem acessar outros métodos e atributos
	 * estáticos da mesma classe, o que faz todo sentido já que dentro de um 
	 * método estático não temos acesso à referência this, pois um método 
	 * estático é chamado através da classe, e não de um objeto.
	 */
	public static int getTotalDeContas() {
		return Conta.totalDeContas;
	}
	
	/* 
	 * Destrutores
	 * -----------
	 * 
	 * Em C, aprendemos que sempre devíamos desalocar (comando free) tudo o que 
	 * alocássemos dinamicamente em memória. Em Java isso não é necessário, pois 
	 * essa linguagem possui um Coletor Automático de Lixo (Garbage Collector), 
	 * o qual é responsável por desalocar tudo aquilo que não é mais utilizado. 
	 * Assim, os objetos que não são mais referenciados por um programa são 
	 * automaticamente desalocados por esse coletor, liberando memória.
	 * 
	 * Em Java, o método finaliza pode ser usado como destrutor de uma classe. 
	 * Quando não é definido um método destrutor para uma classe, Java utiliza 
	 * um método destrutor padrão que não faz nada.
	 * 
	 * O método finalize é raramente utilizado porque pode causar problemas de 
	 * desempenho e há uma incerteza sobre se ele será chamado
	 */
}
