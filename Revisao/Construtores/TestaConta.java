
public class TestaConta {

	public static void main(String[] args) {
	
		Conta c1, c2, c3;
		
		c1 = new Conta(1, "José", 100);
		
		System.out.println("Número da conta: " + c1.getNumero());
		System.out.println("Titular da conta: " + c1.getNome());
		System.out.println("Saldo da conta: " + c1.getSaldo());
		System.out.println();
		System.out.println("O banco agora tem " + Conta.getTotalDeContas() + " conta.");
		System.out.println();
		
		c2 = new Conta(2, "Maria");
		
		System.out.println("Número da conta: " + c2.getNumero());
		System.out.println("Titular da conta: " + c2.getNome());
		System.out.println("Saldo da conta: " + c2.getSaldo());
		System.out.println();
		System.out.println("O banco agora tem " + Conta.getTotalDeContas() + " contas.");
	}

}
