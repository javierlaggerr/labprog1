
public class TestaConta {

	public static void main(String[] args) {
		
		/* Criando e usando um objeto
		 * - Para criar (construir, instanciar) um objeto, a palavra chave 'new'
		 * deve ser usada seguida da classe, com o uso do parênteses.
		 * - O 'new' aloca memória para o objeto e devolve um valor referência.
		 * - Para poder acessar o objeto, precisamos de uma variável que guarde
		 * a referência ao objeto.
		 */
		Conta minhaConta;
		minhaConta = new Conta();
		
		/* Acessando atributos no objeto
		 * - Para acessar os atributos do objeto, devemos usar o ponto '.' após
		 * a variável de referência seguido do atributo que queremos acessar.
		 */
		minhaConta.proprietario = "Willian";
		minhaConta.saldo = 1000;
		
		System.out.println("Saldo inicial: " + minhaConta.saldo);
		
		/* Acessando métodos no objeto
		 * - Para acessar os métodos do objeto, devemos usar o ponto '.' após
		 * a variável de referência seguido do método que queremos acessar.
		 * - Caso o método tenha parâmetros, estes podem ser passados utilizando
		 * parênteses e vírgula em caso de vários parâmetros.
		 */
		minhaConta.deposito(500);
		System.out.println("Saldo após o depósito: " + minhaConta.saldo);
		
		if (minhaConta.saque(750))
			System.out.println("Saldo após o saque: " + minhaConta.saldo);
		else
			System.out.println("Saldo insuficiente para saque!");

		
		Conta meuSonho;
		meuSonho = new Conta();
		meuSonho.saldo = 5000000;
		System.out.println("Saldo da conta dos meus sonhos: " + meuSonho.saldo);
		
		/* Objetos são acessados por referência
		 * - Quando declaramos uma variável para associar a um objeto, esta 
		 * variável não guarda o objeto e sim uma maneira de acessá-lo, chamada
		 * de referência.
		 * IMPORTANTE: Uma variável nunca é um objeto.
		 */
		minhaConta = meuSonho;
		System.out.println("Saldo da minha conta (dos meus sonhos): " + minhaConta.saldo);
	}

}
