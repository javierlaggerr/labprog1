

/* Classes em Java:
 * - Uma classe em Java é sempre declarada com a palavra-chave 'class' seguida do nome
 * da classe. 
 * - O nome da classe: 
 * 		o não pode conter espaços, 
 * 		o deve sempre ser iniciada por uma letra, 
 * 		o não pode conter acentos 
 * 		o e pode conter números, contanto que apareçam depois de uma letra.
 * - Java é case sensitive e 'Class' é diferente de 'class'.
 * - Tradicionalmente os nomes de classes começam com caracteres maiúsculos e
 * alternam entre palavras. Exemplo: ContaBancaria, CadastroDeCliente. 
 */
public class Conta {
	
	/* Atributos
	 * - Atributos em Java devem ser declarados dentro do corpo da classe.
	 * - Seguem as mesmas regras de nomes de classes.
	 * - Tradicionalmente nomes de atributos e variáveis começam com caracteres
	 * minúsculos, alternando com maiúsculos entre palavras. Exemplo: dia, 
	 * nomeDoAluno, estadoInicial.
	 * - Atributos podem ter valores padrão. Quando a classe for instanciada,
	 * o atributo receberá o valor padrão automaticamente.
	 */
	int numero;
	String proprietario;
	double saldo;
	double limite = 1000;
	
	/* Métodos
	 * - Nomes de métodos seguem as mesmas regras de nomes de atributos.
	 * - Nomes de métodos refletem ações que são efetuadas nos atributos da classe
	 * e/ou nos valores passados como argumentos para estes métodos.
	 * - Tradicionalmente os nomes de métodos começam com caracteres minúsculos
	 * e alternam para maiúsculos entre palavras. Exemplo: mostraDados, acende.
	 * - Métodos não podem ser criados dentro de outros métodos, nem fora da
	 * classe a qual ele pertence.
	 */
	void deposito(double valor) {
		this.saldo += valor;
	}
	
	/* Métodos (continuação)
	 * - Cada método deve ter, na sua declaração, um tipo ou classe de retorno,
	 * correspondente ao valor que o método deve retornar.
	 * - Métodos que retornam algum valor diferente de void devem ter, em seu
	 * corpo, a palavra chave 'return' seguida de uma constante ou variável do
	 * tipo ou classe que foi declarada como sendo a de retorno do método.
	 * - Métodos podem ter listas de argumentos, ou seja, variáveis contendo
	 * valores que podem ser usados pelos métodos para efetuar suas operações.
	 */
	boolean saque(double valor) {
		if (this.saldo < valor)
			return false;
		else {
			this.saldo = this.saldo - valor;
			return true;
		}
	}
	
	/* Escopo
	 * - Atributos declarados em um classe são válidos por toda a classe, 
	 * independente de onde são declarados.
	 * - Variáveis e instâncias declaradas dentro de métodos só serão válidas
	 * dentro destes métodos.
	 * - Variáveis e instâncias declaradas dentro de blocos de comandos só serão 
	 * válidas dentro destes blocos.
	 * - Variáveis passadas como argumentos para os métodos só serão válidas
	 * dentro dos métodos.
	 */
}
