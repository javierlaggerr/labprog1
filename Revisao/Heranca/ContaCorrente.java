/*
 * Em Java, para dizer que uma classe herda os métodos e atributos de outra 
 * classe usamos a palavra reservada 'extends' durante a definição da classe
 * filha.
 * 
 * Uma classe filha tem como objetivo definir um tipo mais especializado da
 * classe pai. Por esse motivo, ao mecanismo de criar novas classes herdando 
 * de outras é dado o nome de especialização.
 * 
 * A classe pai, por outro lado, generaliza os conceitos das classes filhas.
 * 
 * Quando visualizamos uma hierarquia partindo da classe pai para filhas, 
 * dizemos que houve uma especialização da superclasse. Quando visualizamos 
 * partindo das classes filhas para as classes ancestrais, dizemos que houve 
 * uma generalização das subclasses.
 */
public class ContaCorrente extends Conta{

	private double limite;
	
	/* 
	 * Caso não se tenha definido um construtor na superclasse, não será 
	 * obrigado a definir construtores para as subclasses, pois Java utilizará 
	 * o construtor padrão para a superclasse e para as subclasses. Porém, 
	 * caso haja algum construtor definido na superclasse, obrigatoriamente 
	 * será necessárió criar ao menos um construtor para cada subclasse. 
	 * Vale ressaltar que os construtores das subclasses utilizarão os 
	 * construtores das superclasses pelo uso da palavra reservada super.
	 */
	public ContaCorrente(int numero, String nome, double limite) {
		this.numero = numero;
		this.nome = nome;
		this.limite = limite;
		this.tipo = "Conta Corrente";
	}
	
	public boolean saque(double valor) {
		if (valor <= this.limite + this.saldo) {
			this.saldo -= valor;
			return true;
		} else {
			return false;
		}
	}
	
	public void setLimite(double valor) {
		this.limite = valor;
	}
	
	public double getLimite() {
		return this.limite;
	}
}
