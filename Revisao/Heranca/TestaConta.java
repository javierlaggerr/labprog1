
public class TestaConta {

	public static void main(String[] args) {
	
		ContaCorrente c1; 
		ContaPoupanca c2;
		
		c1 = new ContaCorrente(1, "José", 100);
		
		System.out.println("Número da conta: " + c1.getNumero());
		System.out.println("Titular da conta: " + c1.getNome());
		System.out.println("Saldo da conta: " + c1.getSaldo());
		System.out.println("Limite da conta: " + c1.getLimite());
		System.out.println("Tipo da conta: " + c1.getTipo());
		System.out.println();
		System.out.println("O banco agora tem " + Conta.getTotalDeContas() + " conta.");
		System.out.println();
		
		c2 = new ContaPoupanca(2, "Maria");
		
		System.out.println("Número da conta: " + c2.getNumero());
		System.out.println("Titular da conta: " + c2.getNome());
		System.out.println("Saldo da conta: " + c2.getSaldo());
		System.out.println("Tipo da conta: " + c2.getTipo());
		System.out.println();
		System.out.println("O banco agora tem " + Conta.getTotalDeContas() + " contas.");
		
		System.out.println();
		
		c1.deposito(1000);
		c2.deposito(1000);
		
		if (c1.saque(1100)) {
			System.out.println("Sacou de c1!");
		} else {
			System.out.println("Saldo insuficiente em c1!");
		}
		
		if (c2.saque(1000)) {
			System.out.println("Sacou de c2!");
		} else {
			System.out.println("Saldo insuficiente em c2!");
		}
	}

}
