
public class Conta {

	/*
	 * Atributos protected
	 * --------- ---------
	 * 
	 * Quando estudamos encapsulamento aprendemos que devemos preferencialmente 
	 * manter os atributos com nível de acesso privado (private) de forma que 
	 * para acessá-los outras classes precisem utilizar métodos.
	 * 
	 * Mas, vimos também que há um nível de acesso protegido (protected) que 
	 * faz com que o atributo se comporte como público para classes da mesma 
	 * hierarquia ou do mesmo pacote e como privado para as demais classes. 
	 * Definir alguns atributos da superclasse como protected pode trazer algumas 
	 * facilidades para implementar métodos das subclasses.
	 */
	protected static int totalDeContas;
	protected int numero;
	protected String nome;
	protected double saldo;
	protected String tipo;
	
	/* 
	 * Herança
	 * -------
	 * 
	 * Uma das vantagens das linguagens orientadas a objeto é a possibilidade 
	 * de se reutilizar código. Mas, quando falamos em reutilização de código, 
	 * precisamos de recursos muito mais poderosos do que simplesmente copiar 
	 * e alterar o código.
	 * 
	 * Um dos conceitos de orientação a objetos que possibilita a reutilização 
	 * de código é o conceito de herança. Pelo conceito de herança é possível 
	 * criar uma nova classe a partir de outra classe já existente.
	 * 
	 * Herança é um mecanismo que permite que uma classe herde todo o 
	 * comportamento e os atributos de outra classe.
	 * 
	 *  Em uma herança, a classe da qual outras classes herdam é chamada de 
	 *  classe pai, classe base ou superclasse. Já a classe que herda de uma 
	 *  outra é chamada de classe filha, classe derivada ou subclasse.
	 *  
	 */
	public Conta() {
		Conta.totalDeContas = Conta.totalDeContas + 1;
	}
	
	public void deposito(double valor) {
		this.saldo = valor;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public double getSaldo() {
		return this.saldo;
	}

	public String getTipo() {
		return this.tipo;
	}

	public static int getTotalDeContas() {
		return Conta.totalDeContas;
	}
}
