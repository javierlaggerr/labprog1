
public class Casting {
	
	public static void main(String[] args){
		/*
		 * Casting e Promoção
		 * ------- - --------
		 * 
		 * Alguns valores são incompatíveis se você tentar fazer 
		 * uma atribuição direta. Enquanto um número real costuma 
		 * ser representado em uma variável do tipo double, tentar 
		 * atribuir ele a uma variável int não funciona porque é 
		 * um código que diz: "i deve valer d”, mas não se sabe se 
		 * d realmente é um número inteiro ou não.
		 * 
		 * 
		 */

		double d = 3.1415;
		//int i = d; // não compila
		
		System.out.println("Valor de d: " + d);
		//System.out.println("Valor de i: " + i);
		
		/*
		 * O mesmo ocorre no seguinte trecho:
		 */
		
		//int i = 3.14;
		
		//System.out.println("Valor de i: " + i);
		
		/*
		 * O mais interessante, é que nem mesmo o seguinte código 
		 * compila:
		 */
		
		//double d = 5; // ok, o double pode conter um número inteiro
		//int i = d; // não compila	
		
		System.out.println("Valor de d: " + d);
		//System.out.println("Valor de i: " + i);
		
		/*
		 * Apesar de 5 ser um bom valor para um int , o compilador 
		 * não tem como saber que valor estará dentro desse double 
		 * no momento da execução. Esse valor pode ter sido digitado 
		 * pelo usuário, e ninguém vai garantir que essa conversão 
		 * ocorra sem perda de valores. 
		 * 
		 * Já no caso a seguir, é o contrário:
		 */
		
		int i2 = 5;
		double d2 = i2;
		
		System.out.println("Valor de i2: " + i2);
		System.out.println("Valor de d2: " + d2);
		
		/*
		 * O código acima compila sem problemas, já que um double 
		 * pode guardar um número com ou sem ponto flutuante. Todos 
		 * os inteiros representados por uma variável do tipo int 
		 * podem ser guardados em uma variável double, então não 
		 * existem problemas no código acima. Às vezes, precisamos 
		 * que um número quebrado seja arredondado e armazenado num 
		 * número inteiro. Para fazer isso sem que haja o erro de 
		 * compilação, é preciso ordenar que o número quebrado seja 
		 * moldado (casted) como um número inteiro. Esse processo 
		 * recebe o nome de casting.
		 */
		
		double d3 = 3.14;
		int i3 = (int) d3;
		
		System.out.println("Valor de d3: " + d3);
		System.out.println("Valor de d3: " + i3);
		
		/*
		 * O casting foi feito para moldar a variável d3 como um int. 
		 * O valor de i agora é 3.
		 * 
		 * O mesmo ocorre entre valores int e long.
		 */
		
		long x = 10000;
		//int i = x; // não compila, pois pode estar perdendo informação
		
		System.out.println("Valor de x: " + x);
		
		/*
		 * E, se quisermos realmente fazer isso, fazemos o casting:
		 */
		
		//int i4 = (int) x;
		
		//System.out.println("Valor de i4: " + i4);
		
		/*
		 * Casos não tão comuns de casting e atribuição
		 * ----- --- --- ------ -- ------- - ----------
		 */
		
		//float f = 0.0;
		
		//System.out.println("Valor de f: " + f);
		
		/*
		 * O código acima não compila pois todos os literais com 
		 * ponto flutuante são considerados double pelo Java. E 
		 * float não pode receber um double sem perda de informação, 
		 * para fazer isso funcionar podemos escrever o seguinte:
		 */
		
		//float f = 0.0f;
		
		//System.out.println("Valor de f: " + f);
		
		/*
		 * A letra f, que pode ser maiúscula ou minúscula, indica 
		 * que aquele literal deve ser tratado como float.
		 * 
		 * Outro caso, que é mais comum:
		 */
		
		double d5 = 5;
		float f5 = 3;
		float x5 = f5 + (float) d5;
		
		System.out.println("Valor de x5: " + x5);
		
		/*
		 * Você precisa do casting porque o Java faz as contas e vai 
		 * armazenando sempre no maior tipo que apareceu durante as 
		 * operações, no caso o double.
		 * 
		 * E, uma observação: no mínimo, o Java armazena o resultado 
		 * em um int, na hora de fazer as contas.
		 * 
		 * Até casting com variáveis do tipo char podem ocorrer. O 
		 * único tipo primitivo que não pode ser atribuído a nenhum 
		 * outro tipo é o boolean.
		 *
		 *Castings possíveis
		 *-------- ---------
		 *
		 *Na figura na pasta de coteúdo estão relacionados todos os 
		 *casts possíveis na linguagem Java, mostrando a conversão de
		 *um valor para outro. A indicação Impl. quer dizer que aquele 
		 *cast é implícito e automático, ou seja, você não precisa 
		 *indicar o cast explicitamente (lembrando que o tipo boolean 
		 *não pode ser convertido para nenhum outro tipo).
		 */
	}
	
}
