
/* 
 * Atributos e métodos estáticos
 * --------- - ------- ---------
 * 
 * Atributos estáticos são atributos que contêm informações inerentes a 
 * uma classe e não a um objeto em específico. Por isso são também 
 * conhecidos como atributos ou variáveis de classe.
 * 
 * Por exemplo, suponha que quiséssemos ter um atributo que indicasse a 
 * quantidade de contas criadas. Esse atributo não seria inerente a uma 
 * conta em específico, mas a todas as contas. Assim, seria definido como 
 * um atributo estático. Para definir um atributo estático em Java, basta 
 * colocar a palavra static entre o qualificador e o tipo do atributo.
 * 
 * O mesmo conceito é válido para métodos. Métodos estáticos são inerentes 
 * à classe e, por isso, não nos obrigam a instanciar um objeto para que 
 * possamos utilizá-los. Para definir um método como estático, basta utilizar 
 * a palavra static, a exemplo do que acontece com atributos. Para utilizar 
 * um método estático é preciso utilizar o nome da classe acompanhado pelo 
 * nome do método.
 * 
 * Métodos estáticos são muito utilizados em classes do Java que proveem 
 * determinados serviços. Por exemplo, Java fornece na classe Math uma série 
 * de métodos estáticos que fazem operações matemáticas como: raiz quadrada 
 * (sqrt), valor absoluto (abs), seno (sin) entre outros.
 * 
 */
public class Conta {

	private static int totalDeContas;
    private int numero;
    private String titular;
    private String senha;
    private double saldo;
    private double limite;
    
    public static int getTotalDeContas() {
    	return Conta.totalDeContas;
    }
    
    public static void incrementaConta() {
    	Conta.totalDeContas ++;
    }
    
    public void setLimite(double valor) {
    	this.limite = valor;
    }
    
    public double getLimite() {
    	return this.limite;
    }
    
    /*
     * A classe String
     * - ------ ------
     * 
     * Java não conta com um tipo primitivo para trabalhar com cadeia de 
     * caracteres. Para isso temos em Java a classe String.
     * 
     * Para criar uma instância de String, não precisamos utilizar o 
     * operador new, como acontece com as outras classes. Para instanciar 
     * um objeto do tipo String, basta declarar uma variável desse tipo e 
     * iniciá-la com um valor. É importante saber também que objetos da 
     * classe String podem ser concatenados utilizando o operador “+”.
     * 
     * Para comparar se os valores de duas Strings são iguais, utilizamos 
     * o método “equals” e não o operador “==” que é utilizado para tipos 
     * primitivos.
     * 
     * A classe String conta ainda com diversos métodos muito úteis, 
     * dentre os quais podemos destacar:
     * 
     * - length: retorna o tamanho ( tipo int ) de uma String.
     * - charAt: retorna o caracter (char) da String que se localiza no 
     * índice passado como parâmetro. Vale ressalta que o primeiro índice 
     * de uma String é o índice zero.
     * - toUppperCase: retorna uma String com todas as letras maiúsculas 
     * a partir da String que chamou o método.
     * - toLowerCase: retorna uma String com todas as letras minúsculas a 
     * partir da String que chamou o método.
     * - trim: retorna uma String sem espaços em branco no início e no 
     * final dela, a partir da String que chamou o método.
     * - replace: Retorna uma String com substrings trocadas, a partir da 
     * String que chamou o método. As trocas são feitas de acordo com os 
     * parâmetros do método: onde aparecer a substring1 será substituída 
     * pela substring 2.
     */
    public int setSenha(String senha, String _senha) {
    	if (senha.length() < 8)
    		return 1;
    	
    	if (senha.equals(_senha)) {
    		this.senha = senha;
    		return 0;
    	}
    	else
    		return 2;
    }
    
    public boolean saque(double valor, String senha) {
    	if (this.senha.equals(senha)) {
    		this.saldo = this.saldo - valor;
    		return true;
    	}
    	else
    		return false;
    }
    
    public void deposito(double valor) {
    	this.saldo = this.saldo + valor;
    }
    
    public double getSaldo() {
    	return this.saldo;
    }
}
