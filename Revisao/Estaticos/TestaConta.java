
public class TestaConta {

	public static void main(String[] args) {
        Conta minhaConta = new Conta();
        Conta.incrementaConta();
        
        System.out.println("O banco tem "+Conta.getTotalDeContas()+" conta.");
        
        minhaConta.setLimite(1000);
        
        switch (minhaConta.setSenha("teste1234", "teste1234")) {
        	case 0:
        		System.out.println("Senha gravada com sucesso!");
        		break;
        	case 1:
        		System.out.println("Senha menor que 8 caracteres!");
        		break;
        	case 2:
        		System.out.println("Senha não bate!");
        		break;
        }
        
        minhaConta.deposito(1000);
        
        if (minhaConta.saque(500, "teste1234"))
        	System.out.println("Saque realizado! Saldo atual: "+minhaConta.getSaldo());
        else
        	System.out.println("Senha incorreta!");

        if (minhaConta.saque(500, "teste"))
        	System.out.println("Saque realizado! Saldo atual: "+minhaConta.getSaldo());
        else
        	System.out.println("Senha incorreta!");

        
        Conta novaConta1 = new Conta();
        Conta.incrementaConta();
        
        System.out.println("O banco tem "+Conta.getTotalDeContas()+" contas.");
        
        Conta novaConta2 = new Conta();
        Conta.incrementaConta();

        System.out.println("O banco tem "+Conta.getTotalDeContas()+" contas.");
	}
}
