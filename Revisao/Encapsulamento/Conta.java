
/* 
 * Encapsulamento
 * --------------
 * 
 * Encapsulamento é uma técnica utilizada para restringir o acesso
 * a variáveis (atributos), métodos ou até à própria classe. 
 * Os detalhes da implementação ficam ocultos ao usuário da classe, 
 * ou seja, o usuário passa a utilizar os métodos de uma classe sem 
 * se preocupar com detalhes sobre como o método foi implementado 
 * internamente.
 * 
 * A ideia do encapsulamento na programação orientada a objetos é 
 * que não seja permitido acessarmos diretamente as propriedades de 
 * um objeto. Nesse caso, precisamos operar sempre por meio dos 
 * métodos pertencentes a ele. A complexidade de um objeto é 
 * escondida, portanto, pela abstração de dados que estão “por trás” 
 * de suas operações.
 * 
 * Os qualificadores de acesso nos permitem modificar o nível de 
 * acesso aos atributos, aos métodos e até mesmo às classes. São três 
 * os possíveis qualificadores de acesso em Java:
 * 
 * - public (público): indica que o método ou o atributo é acessível 
 * por qualquer classe, ou seja, que podem ser usados por qualquer 
 * classe, independentemente de estarem no mesmo pacote ou estarem 
 * na mesma hierarquia;
 * 
 * - private (privado): indica que o método ou o atributo é 
 * acessível apenas pela própria classe, ou seja, só podem ser 
 * utilizados por métodos da própria classe;
 * 
 * - protected (protegido): indica que o atributo ou o método é 
 * acessível pela própria classe, por classes do mesmo pacote ou 
 * classes da mesma hierarquia (tratada em herança).
 * 
 * De forma geral, a ideia do encapsulamento é a de que cada classe 
 * é responsável por controlar seus atributos; portanto, ela deve 
 * julgar se aquele novo valor é válido ou não. Essa validação não 
 * deve ser controlada por quem esta utilizando a classe, e sim pela 
 * própria classe, centralizando essa responsabilidade e facilitando 
 * futuras mudanças no sistema. Assim, em geral, os atributos de uma 
 * classe devem ser privados e deve haver métodos públicos que 
 * permitam o acesso a eles.
 * 
 * Os atributos private de uma classe só podem ser manipulados pelos 
 * métodos da classe. Portanto, um cliente de um objeto – isto é, 
 * qualquer classe que utilize o objeto – deverá chamar os métodos 
 * public da classe para manipular os campos private de um objeto da 
 * classe.
 * 
 * Por padrão, os atributos “encapsulados” devem ter um método que 
 * obtenha o valor atual do atributo (método get) e um método que 
 * altere o valor do atributo (método set).
 * 
 * Vale ressaltar que o qualicador private também pode ser usado para 
 * modicar o acesso a um método quando este existe apenas para 
 * auxiliar a própria classe e não queremos que outras pessoas o 
 * utilizem.
 */
public class Conta {

    int numero;
    String titular;
    double saldo;
    double limite;
    
    void saca(double valor) {
        this.saldo = this.saldo - valor;
    }
    
    void deposita(double valor) {
    	this.saldo = this.saldo + valor;
    }
}
